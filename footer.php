<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
		<div id="footer-container">
			<footer id="footer">
      <div class="footer-contatos">
        <div class="expanded row">
          <div class="medium-10 small-centered columns">
            <div class="row expanded">
              <div class="medium-8 columns">
                <div class="row small-up-2 medium-up-2 large-up-2">

                  <div class="column column-block">
                    <address>
                      <span class="localizacao"><strong>SÃO PAULO/SP</strong></span><br>
                      <span class="telefone">+55 11 4306-8900</span><br>
                      <span class="endereco">Av. Brigadeiro Luis Antônio, 2503 - 3° andar – Jardim Paulista São Paulo/SP CEP: 01.401-000</span><br>
                      <a target="_blank" href="https://www.google.com.br/maps/dir//Lecom+-+Unidade+S%C3%A3o+Paulo%2FCampinas+-+Av+Brigadeiro+Lu%C3%ADs+Antonio,+2503+-+Vila+Clementino,+Jardim+Paulista,+S%C3%A3o+Paulo+-+SP,+01401-002/@-23.569472,-46.650363,17z/data=!4m13!1m4!3m3!1s0x94ce5a3116c7afd7:0x5cd102ce6846c5ad!2sLecom+-+Unidade+S%C3%A3o+Paulo%2FCampinas!3b1!4m7!1m0!1m5!1m1!1s0x94ce5a3116c7afd7:0x5cd102ce6846c5ad!2m2!1d-46.650363!2d-23.569472?hl=pt-BR" class="link-como-chegar">COMO CHEGAR</a>
                    </address>
                  </div>

                  <div class="column column-block">
                    <address>
                      <span class="localizacao"><strong>BAURU/SP</strong></span><br>
                      <span class="telefone">+55 14 4009-8900</span><br>
                      <span class="endereco">Rua Manoel Bento da Cruz, 11- 29 – Centro Bauru/SP CEP: 17015-172</span><br>
                      <a target="_blank" href="https://www.google.com.br/maps/dir//Lecom+-+Rua+Manoel+Bento+da+Cruz,+11-+29+-+Centro,+Bauru+-+SP,+17015-172/@-22.327047,-49.066114,17z/data=!4m13!1m4!3m3!1s0x94bf67a63509ddad:0x1528330435f7528!2sLecom!3b1!4m7!1m0!1m5!1m1!1s0x94bf67a63509ddad:0x1528330435f7528!2m2!1d-49.066114!2d-22.327047?hl=pt-BR" class="link-como-chegar">COMO CHEGAR</a>
                    </address>
                  </div>

                  <!--<div class="column column-block">
                    <address>
                      <span class="localizacao"><strong>JOINVILLE/SC</strong></span><br>
                      <span class="telefone">+55 47 3278-8900</span><br>
                      <span class="endereco">R. Henrique Meyer, 280 - Centro Joinville/SC CEP: 89.201-405</span><br>
                      <a target="_blank" href="https://www.google.com.br/maps/dir//Lecom+-+Rua+Henrique+Meyer,+280+-+Centro,+Joinville+-+SC,+89201-405,+Brasil/@-26.302594,-48.851059,17z/data=!4m12!1m3!3m2!1s0x94deb0661f96849d:0x4a1008c584c098e0!2sLecom!4m7!1m0!1m5!1m1!1s0x94deb0661f96849d:0x4a1008c584c098e0!2m2!1d-48.851059!2d-26.302594?hl=pt-BR" class="link-como-chegar">COMO CHEGAR</a>
                    </address>
                  </div>-->

                  <div class="column column-block">
                    <address>
                      <span class="localizacao"><strong>RIO DE JANEIRO/RJ</strong></span><br>
                      <span class="telefone">+55 21 4042-1380</span><br>
                      <span class="endereco">R. Santa Luzia, 651 - 29º andar, Sala 2903, Centro - CEP: 20.021-903</span><br>
                      <a target="_blank" href="https://www.google.com.br/maps/dir//Lecom+-+Rua+Henrique+Meyer,+280+-+Centro,+Joinville+-+SC,+89201-405,+Brasil/@-26.302594,-48.851059,17z/data=!4m12!1m3!3m2!1s0x94deb0661f96849d:0x4a1008c584c098e0!2sLecom!4m7!1m0!1m5!1m1!1s0x94deb0661f96849d:0x4a1008c584c098e0!2m2!1d-48.851059!2d-26.302594?hl=pt-BR" class="link-como-chegar">COMO CHEGAR</a>
                    </address>
                  </div>

                  <div class="column column-block">
                    <address>
                      <span class="localizacao"><strong>BRASÍLIA/DF</strong></span><br>
                      <span class="telefone">+55 61 4042-1380</span><br>
                      <span class="endereco">SGAN 607. Bloco B. Sala 311. Edifícil Medical Center. CEP: 70.850-070</span><br>
                      <a target="_blank" href="https://www.google.com.br/maps/dir//UnB+Setor+de+Grandes+%C3%81reas+Norte+607+-+Asa+Norte,+Brasilia+-+Federal+District,+70297-400/@-15.7644004,-47.874873,18z/data=!4m16!1m7!3m6!1s0x935a3bb125f03ea9:0x43e904023b55a6e9!2sUnB+Setor+de+Grandes+%C3%81reas+Norte+607+-+Asa+Norte,+Brasilia+-+Federal+District,+70297-400!3b1!8m2!3d-15.7647144!4d-47.875464!4m7!1m0!1m5!1m1!1s0x935a3bb125f03ea9:0x43e904023b55a6e9!2m2!1d-47.875464!2d-15.7647144" class="link-como-chegar">COMO CHEGAR</a>
                    </address>
                  </div>

                </div>
              </div>
              <div class="medium-4 columns">
                <div class="redes-sociais">
                  <a href="https://www.youtube.com/user/lecomsa" title="Canal no YouTube da Lecom" target="_blank">
                    <span class="fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                      <i class="fa fa-youtube-play fa-stack-1x"></i>
                    </span>
                  </a>

                  <a href="https://www.facebook.com/lecomtecnologia" title="Facebook da Lecom" target="_blank">
                    <span class="fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                      <i class="fa fa-facebook fa-stack-1x"></i>
                    </span>
                  </a>

                  <a href="https://www.linkedin.com/company/lecom" title="LinkedIn da Lecom" target="_blank">
                    <span class="fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                      <i class="fa fa-linkedin fa-stack-1x"></i>
                    </span>
                  </a>

                  <a href="http://www.bloglecom.com.br/" title="Blog da Lecom" target="_blank">
                    <span class="fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                      <i class="fa fa-wordpress fa-stack-1x"></i>
                    </span>
                  </a>

                  <a href="https://twitter.com/LecomTecnologia" title="Twitter da Lecom" target="_blank">
                    <span class="fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                      <i class="fa fa-twitter fa-stack-1x"></i>
                    </span>
                  </a>

                  <a href="http://issuu.com/lecomsa" title="Issu da Lecom" target="_blank">
                    <span class="fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                      <i class="ic-l ic-issu fa-stack-1x"></i>
                    </span>
                  </a>
                </div>
                <div class="software-nacional">
                  <span class="software-text">SOFTWARE </span> <span class="nacional-text">100% NACIONAL</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-aportes">
        <div class="row">
          <div class="small-12 columns small-centered">
            <span>Aportes e contribuição ao crescimento sustentável Lecom:</span> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/identity/incentivos.png" alt="Logos Aportes" width="320" height="29" class="alignnone size-full wp-image-131" /> <span>Desenvolvido por:</span> <a href="http://twodigital.com.br/"><img src="http://lecom2017aceite.lecom.com.br/wp-content/uploads/2017/03/logo-two.svg" alt="Logo TWO Digital" class="alignnone size-full wp-image-132" /></a>
          </div>
        </div>
      </div>
			</footer>
		</div>

		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>

  <!--Start of Zendesk Chat Script-->
  <script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?4ndy3s8Wytm0y2XFWHXgdrSjYfXZUW3U";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
  </script>
  <!--End of Zendesk Chat Script-->

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-712819-1', 'auto');
    ga('send', 'pageview');
  </script>

  <?php if (get_site_url() == "http://www.lecom.com.br" || get_site_url() == "http://lecom.com.br" ):?>
    <script type="text/javascript" async src="https://scripts.lahar.com.br/track-lecomkm0tkmlRy0uklenDIx7vbhqm4SmutO4TXTYX.js"></script>
  <?php endif; ?>

</body>
</html>