<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
  <?php do_action( 'foundationpress_after_body' ); ?>

  <?php do_action( 'foundationpress_layout_start' ); ?>

  <header id="masthead" class="site-header" data-sticky-container>
    <div class="top-bar" data-sticky data-options="marginTop:0;" style="width:100%">
      <div class="row">
        <div class="small-12 columns">
          <div class="top-bar-title">
            <span data-responsive-toggle="responsive-menu" data-hide-for="large">
              <button class="mobile-menu-toggler" type="button" data-toggle><i class="fa fa-bars" aria-hidden="true"></i></button>
            </span>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/identity/logo-lecom.png" srcset="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/identity/logo-lecom@2x.png 2x, <?php echo get_stylesheet_directory_uri(); ?>/assets/images/identity/logo-lecom@3x.png 3x" alt="Lecom"></a>
          </div>
          <div id="responsive-menu">
            <div class="top-bar-left" data-magellan>
              <ul id="menu-top-menu" class="dropdown menu desktop-menu" data-dropdown-menu data-disable-hover="true" data-click-open="true" data-close-on-click-inside="true" data-close-on-click="true">
                <li><a href="#plataforma" data-toggle="menu-plataforma">PLATAFORMA</a>
                  <div class="dropdown-pane xlarge" id="menu-plataforma" data-dropdown data-close-on-click="true">
                    <div class="row small-collapse">
                      <div class="medium-3 columns">
                        <div class="menu-list">
                          <span class="menu-list-title">POR PRODUTO</span>
                          <ul>
                            <li><a href="/bpm-studio">BPM</a></li>
                            <li><a href="/analytics">Analytics</a></li>
                            <li><a href="/workspace-workflow-management">Workflow Management</a></li>
                            <li><a href="/ecm">ECM</a></li>
                          </ul>
                        </div>
                      </div>
                      <div class="medium-2 columns">
                        <div class="menu-list">
                          <span class="menu-list-title">POR APLICAÇÃO</span>
                          <ul>
                            <li><a href="/gestao-de-ti">Gestão de TI</a></li>
                            <li><a href="/marketing">Marketing</a></li>
                            <li><a href="/operacoes">Operações</a></li>
                            <li><a href="/rh">RH</a></li>
                            <li><a href="/supply-chain">Supply chain</a></li>
                            <li><a href="/vendas-e-negocios">Vendas e negócios</a></li>
                          </ul>
                        </div>
                      </div>
                      <div class="medium-2 columns end">
                        <div class="menu-list">
                          <span class="menu-list-title">POR INDÚSTRIA</span>
                          <ul>
                            <li><a href="/governo">Governo</a></li>
                            <li><a href="/saude">Saúde</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="menu-demonstracao">
                      <a href="/tour">
                        <span class="teste-agora">TOUR GUIADO!</span> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/identity/ic-demo-menu.png"  alt="" > <span>Veja como funciona o Lecom BPM</span>
                      </a>
                    </div>
                  </div>
                </li>
                <li><a href="#" onclick="$('#menu-plataforma').foundation('close');">INSIGHTS</a>
                  <ul class="menu">
                    <li><a href="/insights/artigos">ARTIGOS</a></li>
                    <li><a href="/insights/ebooks">E-BOOKS</a></li>
                    <li><a href="/insights/eventos">EVENTOS</a></li>
                    <li><a href="/insights/videos">VÍDEOS</a></li>
                    <li><a href="/insights/webinars">WEBINARS</a></li>
                  </ul>
                </li>
                <li><a href="#" onclick="$('#menu-plataforma').foundation('close');">CLIENTES</a>
                  <ul class="menu">
                    <li><a href="/clientes">HISTÓRIAS DE SUCESSO</a></li>
                    <li><a href="/premiacoes">PREMIAÇÕES</a></li>
                  </ul>
                </li>
                <li><a href="#" onclick="$('#menu-plataforma').foundation('close');">SOBRE</a>
                  <ul class="menu">
                    <li><a href="/sobre-a-lecom">SOBRE NÓS</a></li>
                    <li><a href="/trabalhe-na-lecom/">TRABALHE NA LECOM</a></li>
                  </ul>
                </li>
                <li><a href="/blog">BLOG</a></li>

              </ul>
              <?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) === 'topbar' ) : ?>
                <?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
              <?php endif; ?>
            </div>
            <div class="top-bar-right">
            </div>
          </div>
          <div class="entrega-agil">
            <a href="/entrega-agil-de-processos-digitais" class="link-entrega-agil">
              <span>#ENTREGA</span>AGIL DE
              <span>#PROCESSOS</span>DIGITAIS
            </a>
          </div>
        </div>
      </div>
    </div>
  </header>
  <?php do_action( 'foundationpress_after_header' );


