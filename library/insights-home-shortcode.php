<?php
/**
 * Shortcode for insights list on home
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'list_insights_home' ) ) :
  function list_insights_home() {
    $webinars = get_posts(
                array(
                  'posts_per_page'   => 1,
                  'post_type'        => 'insights',
                  'orderby'         => 'modified',
                  'order'            => 'DESC',
                  'meta_query'       => array(
                                          array(
                                            'key'       => 'insight_type',
                                            'value'     => 'webinar',
                                            'compare'   => 'LIKE',
                                          )
                                        )
                )
              );

    $eventos = get_posts(
                array(
                  'posts_per_page'   => 1,
                  'post_type'        => 'insights',
                  'orderby'         => 'modified',
                  'order'            => 'DESC',
                  'meta_query'       => array(
                                          array(
                                            'key'       => 'insight_type',
                                            'value'     => 'evento',
                                            'compare'   => 'LIKE',
                                          )
                                        )
                )
              );

    $ebooks = get_posts(
                array(
                  'posts_per_page'   => 1,
                  'post_type'        => 'insights',
                  'orderby'         => 'modified',
                  'order'            => 'DESC',
                  'meta_query'       => array(
                                          array(
                                            'key'       => 'insight_type',
                                            'value'     => 'ebook',
                                            'compare'   => 'LIKE',
                                          )
                                        )
                )
              );

    $evento_output = NULL;
    if ($eventos) :
      foreach ( $eventos as $evento ) :
        ob_start(); ?>

          <div class="insight row medium-collapse" data-equalizer data-equalize-on="large">
            <div class="large-8  columns" data-equalizer-watch>
              <div class="insight-image" style="background-image: url('<?php echo get_field("insight_image", $evento->ID) ?>')"></div>
            </div>
            <div class="large-4 columns" data-equalizer-watch>
              <div class="insight-infos">
                <span class="insight-data"><?php echo strtoupper(strftime('%d %b. %Y', strtotime(get_field("insight_date", $evento->ID)))) ?></span><br>
                <span class="insight-tipo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/pages/all/ic-evento.png" alt="Evento" /> Evento</span><br>
                <span class="insight-titulo"><?php echo get_field("insight_title", $evento->ID) ?></span><br>
                <span class="insight-descricao"><?php echo get_field("insight_short_description", $evento->ID) ?></span><br>
                <a href="<?php echo get_field('insight_link', $evento->ID) ?>" target="_blank" class="button secondary"><?php echo get_field("insight_link_text", $evento->ID) ?></a>
              </div>
            </div>
          </div>

      <?php $evento_output .= ob_get_clean();
      endforeach;
    endif;

    $webinar_output = NULL;
    if ($webinars) :
      foreach ( $webinars as $webinar ) :
        ob_start(); ?>

          <div class="insight row medium-collapse" data-equalizer data-equalize-on="large">
            <div class="large-8 large-push-4  columns" data-equalizer-watch>
              <div class="insight-image" style="background-image: url('<?php echo get_field("insight_image", $webinar->ID) ?>')"></div>
            </div>
            <div class="large-4 large-pull-8 columns" data-equalizer-watch>
              <div class="insight-infos">
                <span class="insight-data"><?php echo strtoupper(strftime('%d %b. %Y', strtotime(get_field("insight_date", $webinar->ID)))) ?></span><br>
                <span class="insight-tipo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/pages/all/ic-webinar.png" alt="Webinar" /> WEBINAR</span><br>
                <span class="insight-titulo"><?php echo get_field("insight_title", $webinar->ID) ?></span><br>
                <span class="insight-descricao"><?php echo get_field("insight_short_description", $webinar->ID) ?></span><br>
                <a href="<?php echo get_field('insight_link', $webinar->ID) ?>" target="_blank" class="button secondary"><?php echo get_field("insight_link_text", $webinar->ID) ?></a>
              </div>
            </div>
          </div>

      <?php $webinar_output .= ob_get_clean();
      endforeach;
    endif;

    $ebook_output  = NULL;
    if ($ebooks) :
      foreach ( $ebooks as $ebook ) :
        ob_start(); ?>

          <div class="insight row medium-collapse" data-equalizer data-equalize-on="large">
            <div class="large-8  columns" data-equalizer-watch>
              <div class="insight-image" style="background-image: url('<?php echo get_field("insight_image", $ebook->ID) ?>')"></div>
            </div>
            <div class="large-4 columns" data-equalizer-watch>
              <div class="insight-infos">
                <span class="insight-tipo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/pages/all/ic-ebook.png" alt="Evento" /> E-Book</span><br>
                <span class="insight-titulo"><?php echo get_field("insight_title", $ebook->ID) ?></span><br>
                <span class="insight-descricao"><?php echo get_field("insight_short_description", $ebook->ID) ?></span><br>
                <a href="<?php echo get_field('insight_link', $ebook->ID) ?>" target="_blank" class="button secondary"><?php echo get_field("insight_link_text", $ebook->ID) ?></a>
              </div>
            </div>
          </div>

      <?php $ebook_output .= ob_get_clean();
      endforeach;
    endif;


    wp_reset_postdata();
    $insights_home_output = $evento_output . $webinar_output . $ebook_output;
    return $insights_home_output;
  }
endif;
add_shortcode( 'insights_home', 'list_insights_home' );