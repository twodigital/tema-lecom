<?php
/**
 * Shortcode for case slider
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'list_page_cases_children' ) ) :
  function list_page_cases_children() {
    global $post;
    $postid = $post->ID;
    $pages = get_pages(
                array('child_of' => $postid, 'sort_column' => 'menu_order' )
              );
    $$page_list_output ;



    if ($pages) :
      foreach ( $pages as $page ) :
        //ob_start(); ?>

      <a href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title; ?></a><br>

      <?php //$page_list_output .= ob_get_clean();
      endforeach;
    endif;
    return $page_list_output;
  }
endif;
add_shortcode( 'list_cases', 'list_page_cases_children' );