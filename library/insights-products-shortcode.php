<?php
/**
 * Shortcode for insights list on products pages
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'list_insights_products' ) ) :
  function list_insights_products() {
    global $post;
    $postid = $post->ID;
    $webinars = get_posts(
                array(
                  'posts_per_page'   => 1,
                  'post_type'        => 'insights',
                  'orderby'         => 'modified',
                  'order'            => 'DESC',
                  'meta_query'       => array(
                                          array(
                                            'key'       => 'insight_type',
                                            'value'     => 'webinar',
                                            'compare'   => 'LIKE',
                                          ),
                                          array(
                                            'key'       => 'insight_related_products',
                                            'value'     => '"'.$postid.'"', //está entre aspas pora a comparaçãao do LIKE nao retornar falsos positivos
                                            'compare'   => 'LIKE',
                                          )
                                        )
                )
              );

    $eventos = get_posts(
                array(
                  'posts_per_page'   => 1,
                  'post_type'        => 'insights',
                  'orderby'         => 'modified',
                  'order'            => 'DESC',
                  'meta_query'       => array(
                                          array(
                                            'key'       => 'insight_type',
                                            'value'     => 'evento',
                                            'compare'   => 'LIKE',
                                          ),
                                          array(
                                            'key'       => 'insight_related_products',
                                            'value'     => '"'.$postid.'"', //está entre aspas pora a comparaçãao do LIKE nao retornar falsos positivos
                                            'compare'   => 'LIKE',
                                          )
                                        )
                )
              );

    $ebooks = get_posts(
                array(
                  'posts_per_page'   => 1,
                  'post_type'        => 'insights',
                  'orderby'         => 'modified',
                  'order'            => 'DESC',
                  'meta_query'       => array(
                                          array(
                                            'key'       => 'insight_type',
                                            'value'     => 'ebook',
                                            'compare'   => 'LIKE',
                                          ),
                                          array(
                                            'key'       => 'insight_related_products',
                                            'value'     => '"'.$postid.'"', //está entre aspas pora a comparaçãao do LIKE nao retornar falsos positivos
                                            'compare'   => 'LIKE',
                                          )
                                        )
                )
              );

    $artigos = get_posts(
                array(
                  'posts_per_page'   => 1,
                  'post_type'        => 'insights',
                  'orderby'         => 'modified',
                  'order'            => 'DESC',
                  'meta_query'       => array(
                                          array(
                                            'key'       => 'insight_type',
                                            'value'     => 'artigo',
                                            'compare'   => 'LIKE',
                                          ),
                                          array(
                                            'key'       => 'insight_related_products',
                                            'value'     => '"'.$postid.'"', //está entre aspas pora a comparaçãao do LIKE nao retornar falsos positivos
                                            'compare'   => 'LIKE',
                                          )
                                        )
                )
              );

    $videos = get_posts(
                array(
                  'posts_per_page'   => 1,
                  'post_type'        => 'insights',
                  'orderby'         => 'modified',
                  'order'            => 'DESC',
                  'meta_query'       => array(
                                          array(
                                            'key'       => 'insight_type',
                                            'value'     => 'video',
                                            'compare'   => 'LIKE',
                                          ),
                                          array(
                                            'key'       => 'insight_related_products',
                                            'value'     => '"'.$postid.'"', //está entre aspas pora a comparaçãao do LIKE nao retornar falsos positivos
                                            'compare'   => 'LIKE',
                                          )
                                        )
                )
              );

    $evento_output = NULL;
    if ($eventos) :
      foreach ( $eventos as $evento ) :
        ob_start(); ?>

          <div class="insight row medium-collapse" data-equalizer data-equalize-on="large">
            <div class="large-6 columns" data-equalizer-watch>
              <div class="insight-image" style="background-image: url('<?php echo get_field("insight_image", $evento->ID) ?>')"></div>
            </div>
            <div class="large-6 columns" data-equalizer-watch>
              <div class="insight-infos">
                <span class="insight-tipo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/pages/all/ic-evento.png" alt="Evento" /> EVENTO</span><br>
                <span class="insight-titulo"><?php echo get_field("insight_title", $evento->ID) ?></span>
                <div class="insight-descricao">
                  <p>
                  <?php if (get_field("insight_panelist_name", $evento->ID)) : ?>
                    <strong><em>Com <?php echo get_field("insight_panelist_name", $evento->ID) ?></em></strong>
                  <?php endif; ?>
                  <?php if (get_field("insight_panelist_job", $evento->ID)) : ?>
                    <strong><em> &ndash; <?php echo get_field("insight_panelist_job", $evento->ID) ?></em></strong>
                  <?php endif; ?>
                  </p>
                  <?php echo get_field("insight_short_description", $evento->ID) ?>
                </div>
                <a href="<?php echo get_field('insight_link', $evento->ID) ?>" target="_blank" class="button"><?php echo get_field("insight_link_text", $evento->ID) ?> <i class="fa fa-play-circle" aria-hidden="true"></i></a> <a href="/insights/eventos" target="_blank" class="button dark-blue">VER MAIS EVENTOS <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>

      <?php $evento_output .= ob_get_clean();
      endforeach;
    endif;

    $webinar_output = NULL;
    if ($webinars) :
      foreach ( $webinars as $webinar ) :
        ob_start(); ?>

          <div class="insight row medium-collapse" data-equalizer data-equalize-on="large">
            <div class="large-6 large-push-6 columns" data-equalizer-watch>
              <div class="insight-image" style="background-image: url('<?php echo get_field("insight_image", $webinar->ID) ?>')"></div>
            </div>
            <div class="large-6 large-pull-6 columns" data-equalizer-watch>
              <div class="insight-infos">
                <span class="insight-tipo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/pages/all/ic-webinar.png" alt="Webinar" /> WEBINAR</span><br>
                <span class="insight-titulo"><?php echo get_field("insight_title", $webinar->ID) ?></span>
                <div class="insight-descricao">
                  <p>
                  <?php if (get_field("insight_panelist_name", $webinara->ID)) : ?>
                    <strong><em>Com <?php echo get_field("insight_panelist_name", $webinara->ID) ?></em></strong>
                  <?php endif; ?>
                  <?php if (get_field("insight_panelist_job", $webinara->ID)) : ?>
                    <strong><em> &ndash; <?php echo get_field("insight_panelist_job", $webinara->ID) ?></em></strong>
                  <?php endif; ?>
                  </p>
                  <?php echo get_field("insight_description", $webinar->ID) ?>
                </div>
                <a href="<?php echo get_field('insight_link', $webinar->ID) ?>" target="_blank" class="button"><?php echo get_field("insight_link_text", $webinar->ID) ?> <i class="fa fa-play-circle" aria-hidden="true"></i></a> <a href="/insights/webinars" target="_blank" class="button dark-blue">VER MAIS WEBINARS <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>

      <?php $webinar_output .= ob_get_clean();
      endforeach;
    endif;

    $ebook_output  = NULL;
    if ($ebooks) :
      foreach ( $ebooks as $ebook ) :
        ob_start(); ?>

          <div class="insight row medium-collapse" data-equalizer data-equalize-on="large">
            <div class="large-6 columns" data-equalizer-watch>
              <div class="insight-image" style="background-image: url('<?php echo get_field("insight_image", $ebook->ID) ?>')"></div>
            </div>
            <div class="large-6 columns" data-equalizer-watch>
              <div class="insight-infos">
                <span class="insight-tipo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/pages/all/ic-ebook.png" alt="E-Book" /> E-BOOK</span><br>
                <span class="insight-titulo"><?php echo get_field("insight_title", $ebook->ID) ?></span>
                <div class="insight-descricao">
                  <p>
                  <?php if (get_field("insight_panelist_name", $ebook->ID)) : ?>
                    <strong><em>Com <?php echo get_field("insight_panelist_name", $ebook->ID) ?></em></strong>
                  <?php endif; ?>
                  <?php if (get_field("insight_panelist_job", $ebook->ID)) : ?>
                    <strong><em> &ndash; <?php echo get_field("insight_panelist_job", $ebook->ID) ?></em></strong>
                  <?php endif; ?>
                  </p>
                  <?php echo get_field("insight_description", $ebook->ID) ?>
                </div>
                <a href="<?php echo get_field('insight_link', $ebook->ID) ?>" target="_blank" class="button"><?php echo get_field("insight_link_text", $ebook->ID) ?> <i class="fa fa-play-circle" aria-hidden="true"></i></a> <a href="/insights/ebooks" target="_blank" class="button dark-blue">VER MAIS E-BOOKS <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>

      <?php $ebook_output .= ob_get_clean();
      endforeach;
    endif;

    $artigo_output = NULL;
    if ($artigos) :
      foreach ( $artigos as $artigo ) :
        ob_start(); ?>

          <div class="insight row medium-collapse" data-equalizer data-equalize-on="large">
            <div class="large-6 large-push-6 columns" data-equalizer-watch>
              <div class="insight-image" style="background-image: url('<?php echo get_field("insight_image", $artigo->ID) ?>')"></div>
            </div>
            <div class="large-6 large-pull-6 columns" data-equalizer-watch>
              <div class="insight-infos">
                <span class="insight-tipo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/pages/all/ic-artigo.png" alt="Artigo" /> ARTIGO</span><br>
                <span class="insight-titulo"><?php echo get_field("insight_title", $artigo->ID) ?></span>
                <div class="insight-descricao">
                  <p>
                  <?php if (get_field("insight_panelist_name", $artigo->ID)) : ?>
                    <strong><em>Com <?php echo get_field("insight_panelist_name", $artigo->ID) ?></em></strong>
                  <?php endif; ?>
                  <?php if (get_field("insight_panelist_job", $artigo->ID)) : ?>
                    <strong><em> &ndash; <?php echo get_field("insight_panelist_job", $artigo->ID) ?></em></strong>
                  <?php endif; ?>
                  </p>
                  <?php echo get_field("insight_description", $artigo->ID) ?>
                </div>
                <a href="<?php echo get_field('insight_link', $artigo->ID) ?>" target="_blank" class="button"><?php echo get_field("insight_link_text", $artigo->ID) ?> <i class="fa fa-play-circle" aria-hidden="true"></i></a> <a href="/insights/artigos" target="_blank" class="button dark-blue">VER MAIS ARTIGOS <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>

      <?php $artigo_output .= ob_get_clean();
      endforeach;
    endif;

    $video_output  = NULL;
    if ($ebooks) :
      foreach ( $videos as $video ) :
        ob_start(); ?>

          <div class="insight row medium-collapse" data-equalizer data-equalize-on="large">
            <div class="large-6 columns" data-equalizer-watch>
              <div class="insight-image" style="background-image: url('<?php echo get_field("insight_image", $video->ID) ?>')"></div>
            </div>
            <div class="large-6 columns" data-equalizer-watch>
              <div class="insight-infos">
                <span class="insight-tipo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/pages/all/ic-video.png" alt="Vídeo" /> Vídeo</span><br>
                <span class="insight-titulo"><?php echo get_field("insight_title", $video->ID) ?></span>
                <div class="insight-descricao">
                  <p>
                    <?php if (get_field("insight_panelist_name", $video->ID)) : ?>
                      <strong><em>Com <?php echo get_field("insight_panelist_name", $video->ID) ?></em></strong>
                    <?php endif; ?>
                    <?php if (get_field("insight_panelist_job", $video->ID)) : ?>
                      <strong><em> &ndash; <?php echo get_field("insight_panelist_job", $video->ID) ?></em></strong>
                    <?php endif; ?>
                  </p>
                  <?php echo get_field("insight_description", $video->ID) ?>
                </div>
                <a href="<?php echo get_field('insight_link', $video->ID) ?>" target="_blank" class="button"><?php echo get_field("insight_link_text", $video->ID) ?> <i class="fa fa-play-circle" aria-hidden="true"></i></a> <a href="/insights/videos" target="_blank" class="button dark-blue">VER MAIS VÍDEOS <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>

      <?php $video_output .= ob_get_clean();
      endforeach;
    endif;


    wp_reset_postdata();
    $insights_products_output = $evento_output . $webinar_output . $ebook_output . $artigo_output . $video_output;
    return $insights_products_output;
  }
endif;
add_shortcode( 'insights_products', 'list_insights_products' );