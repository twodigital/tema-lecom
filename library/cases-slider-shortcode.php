<?php
/**
 * Shortcode for case slider
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'create_case_slider' ) ) :
  function create_case_sldier() {
    global $post;
    $postid = $post->ID;
    $cases = get_posts(
                array(
                  'posts_per_page'   => -1,
                  'post_type'        => 'cases_slider',
                  'order'            => 'ASC',
                  'meta_query'       => array(
                                          array(
                                            'key'       => 'case_pages',
                                            'value'     => '"'.$postid.'"', //está entre aspas pora a comparaçãao do LIKE nao retornar falsos positivos
                                            'compare'   => 'LIKE',
                                          )
                                        )
                )
              );
    $case_slider_output;
    $slider_class = (count($cases) > 1) ? " cases-slider hide" : "";


    if ($cases) :
      $case_slider_output = "<div class=\"cases" . $slider_class . "\">";
      foreach ( $cases as $case ) :
        ob_start(); ?>

        <div class="case" style="background-image: url('<?php echo get_field("case_backgorund", $case->ID) ?>');">
          <div class="row">
            <div class="large-7 columns end">
              <blockquote class="case-citacao">
                <p>&ldquo;<?php echo get_field("case_quote", $case->ID) ?>&rdquo;</p>
              </blockquote>
              <div class="row">
                <div class="medium-5 columns">
                  <span class="case-autor"><?php echo get_field("case_author", $case->ID) ?><?php if (get_field("case_author_job", $case->ID)) : ?>,<?php endif; ?> </span>
                  <?php if (get_field("case_author_job", $case->ID)) : ?>
                    <span class="case-autor-cargo"><?php echo get_field("case_author_job", $case->ID) ?></span>
                  <?php endif; ?>

                </div>
                <div class="medium-5 medium-offset-2 columns"><img src="<?php echo get_field("case_client_logo", $case->ID)["url"] ?>" alt="<?php echo get_field("case_client_logo", $case->ID)["alt"] ?>" class="case-logo-cliente" /></div>
              </div>
              <?php if (get_field("case_awarded_client", $case->ID)) : ?>
              <div class="row">
                <div class="medium-5 columns">
                  <?php if (get_field("case_link_type", $case->ID) !== "nada") : ?>
                  <a href="<?php echo get_field("case_link", $case->ID) ?>" class="button secondary expanded">VEJA O CASE COMPLETO</a>
                  <?php endif; ?>
                </div>
                <div class="medium-5 medium-offset-2 columns">
                  <div class="case-premiacao">
                    <img class="case-premiacao-img" src="<?php echo get_field("case_award_logo", $case->ID)["url"] ?>" alt="<?php echo get_field("case_award_logo", $case->ID)["alt"] ?>" />
                    <span class="case-premiacao-texto"><?php echo get_field("case_award_description", $case->ID) ?></span>
                  </div>
                </div>
              </div>
            <?php else : ?>
              <?php if (get_field("case_link_type", $case->ID) !== "nada") : ?>
              <div class="row">
                <div class="medium-8 columns medium-centered">
                  <a href="<?php echo get_field("case_link", $case->ID) ?>" class="button secondary expanded">VEJA O CASE COMPLETO</a>

                </div>
              </div>
              <?php endif; ?>
            <?php endif; ?>
            </div>
          </div>
        </div>

      <?php $case_slider_output .= ob_get_clean();
      endforeach;

      $case_slider_output .= "</div>";
    endif;
    wp_reset_postdata();
    return $case_slider_output;
  }
endif;
add_shortcode( 'case_slider', 'create_case_sldier' );