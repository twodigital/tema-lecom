<?php
    function insertScript(){
        wp_enqueue_script( 'agile-integracao-principal', 'https://d1gwclp1pmzk26.cloudfront.net/agile/agile-cloud.js', '', '1.0.0', true );
        // $handle = 'agile-integracao';
        // $src = get_template_directory_uri() . '/assets/javascript/custom/agile-crm-integracao.js';
        // $deps = array();
        // $ver = '1.0.0';
        // $in_footer = true;
        // wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
    }
    add_action( 'wp_enqueue_scripts', 'insertScript' );

    function form_agile($atts) {

        ob_start();

        extract(shortcode_atts(array(
            'tags' => 'Lecom 2017', // tags para adicionar no lead
            'modeler' =>'false', // se vai fazer download do modeler
            'url_redirect'=>'', // url para redirecionamento, aconselho sempre colocar a página aonde criou o shortcode
            'id_modal'=>'form-agile', // caso ter mais de um model, tem que seta qual o id de cada um, para abrir um de cada vez
            'dois_models'=>'false'// se tiver duas chamadas do shortcode setar como true para carregar somente um script do google.
        ), $atts));

        set_query_var( 'tags_agile', $tags );
        set_query_var( 'modeler', $modeler );
        set_query_var( 'url_redirect', $url_redirect );
        set_query_var( 'id_modal', $id_modal);
        set_query_var( 'dois_models', $dois_models);
        get_template_part( 'template-parts/form_agile_conversao' );

        return ob_get_clean();
    }
    add_shortcode( 'form_agile', 'form_agile' );
?>