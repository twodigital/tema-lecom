<?php
/**
 * Shortcode for insights list on insights pages
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'list_insights' ) ) :
  function list_insights($atts) {

    $atts = shortcode_atts( array(
                          'tipo' => '',
                          'tema' => '',
                          'area_aplicacao' => '',
                          'area_industria' => '',
                          'paged' => 1
                      ), $atts );


    $tema = $area_aplicacao = $area_industria = NULL;

    $tipo           = (empty($_GET['tipo']))           ? $atts['tipo']           : $_GET['tipo'];
    $tema           = (empty($_GET['tema']))           ? $atts['tema']           : $_GET['tema'];
    $area_aplicacao = (empty($_GET['area_aplicacao'])) ? $atts['area_aplicacao'] : $_GET['area_aplicacao'];
    $area_industria = (empty($_GET['area_industria'])) ? $atts['area_industria'] : $_GET['area_industria'];
    $paged          = $atts['paged'];

    $offset = 0; // primeira página vai trazer 4 itens
    if ($paged == 2){
        $offset = 4; // na pag 2 preciso trazer 3  itens mas tirando somente os 4  primeiros
    }elseif($paged > 2){
        $offset = ($paged - 2) * 3 + 4; //nas próximas páginas é usado a seguinte logica (pagina atual - quantidade de paginas rodas antes sem cair nessa condição) * 3 (numero de itens para cada página) + 4 (da primeira página)
    }

    $arr_insights = array(
                  'posts_per_page'  => ($paged > 1 )? 3:4,
                  'post_type'       => 'insights',
                  'orderby'         => 'modified',
                  'order'           => 'DESC',
                  'meta_query'      => array(),
                  'paged'           => $paged,
                  'offset'          => $offset,
                );

    if ($atts['tipo']) {
      array_push($arr_insights['meta_query'], array(
        'key'     => 'insight_type',
        'value'   => $atts['tipo'],
        'compare' => 'LIKE'
        )
      );
    }

    if ($tema) {
      array_push($arr_insights['meta_query'], array(
        'key'     => 'insight_theme',
        'value'   => $tema,
        'compare' => 'LIKE'
        )
      );
    }

    if ($area_aplicacao) {
      array_push($arr_insights['meta_query'], array(
        'key'     => 'insight_application_area',
        'value'   => $area_aplicacao,
        'compare' => 'LIKE'
        )
      );
    }

    if ($area_industria) {
      array_push($arr_insights['meta_query'], array(
        'key'     => 'insight_industry_area',
        'value'   => $area_industria,
        'compare' => 'LIKE'
        )
      );
    }


    $insights = new WP_Query( $arr_insights );

    $insights_output = NULL;

    if ( $insights->have_posts() ) :
      while ( $insights->have_posts() ) :
        $insights->the_post();

        if ($insights->current_post === 0 && $paged === 1) :
          ob_start(); ?>
            <div class="highlight-insight row medium-collapse" data-equalizer data-equalize-on="large">
              <div class="large-6 columns" data-equalizer-watch>
                <div class="insight-image" style="background-image: url('<?php echo get_field("insight_image", $insights->post->ID) ?>')"></div>
              </div>
              <div class="large-6 columns" data-equalizer-watch>
                <div class="insight-infos">
                  <span class="insight-tipo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/pages/all/ic-<?php echo get_field("insight_type", $insights->post->ID) ?>.png" alt="<?php echo ucfirst(get_field("insight_type", $insights->post->ID)) ?>" /> <?php echo strtoupper(get_field("insight_type", $insights->post->ID)) ?></span><br>
                  <span class="insight-titulo"><?php echo get_field("insight_title", $insights->post->ID) ?></span>
                  <div class="insight-descricao">
                    <p>
                    <?php if (get_field("insight_panelist_name", $insights->post->ID)) : ?>
                      <strong><em>Com <?php echo get_field("insight_panelist_name", $insights->post->ID) ?></em></strong>
                    <?php endif; ?>
                    <?php if (get_field("insight_panelist_job", $insights->post->ID)) : ?>
                      <strong><em> &ndash; <?php echo get_field("insight_panelist_job", $insights->post->ID) ?></em></strong>
                    <?php endif; ?>
                    </p>
                    <?php echo get_field("insight_description", $insights->post->ID) ?>
                  </div>
                  <a href="<?php echo get_field('insight_link', $insights->post->ID) ?>" target="_blank" class="button"><?php echo get_field("insight_link_text", $insights->post->ID) ?> <i class="fa fa-play-circle" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          <?php $insights_output .= ob_get_clean();
        else:
          ob_start();
          if ($insights->current_post === 1 && $paged === 1) : ?>
            <div class="insights-grid">
              <div class="row small-up-1 medium-up-2 large-up-3 insight-search-results">
          <?php endif; ?>

                <div class="column insight">
                  <a href="<?php echo get_field('insight_link', $insights->post->ID) ?>" target="_blank">
                    <div class="insight-image"><img src="<?php echo get_field("insight_image", $insights->post->ID) ?>" alt=""></div>
                    <span class="insight-titulo"><?php echo get_field("insight_title", $insights->post->ID) ?></span>
                    <div class="insight-descricao">
                      <p>
                      <?php if (get_field("insight_panelist_name", $insights->post->ID)) : ?>
                        <strong><em>Com <?php echo get_field("insight_panelist_name", $insights->post->ID) ?></em></strong>
                      <?php endif; ?>
                      <?php if (get_field("insight_panelist_job", $insights->post->ID)) : ?>
                        <strong><em> &ndash; <?php echo get_field("insight_panelist_job", $insights->post->ID) ?></em></strong>
                      <?php endif; ?>
                      </p>
                      <?php echo get_field("insight_short_description", $insights->post->ID) ?>
                    </div>
                  </a>
                </div>

            <?php if ($insights->current_post + 1 === $insights->post_count) : ?>
              </div>
            </div>
              <?php if ($insights->max_num_pages > 1 && $paged === 1) : ?>
                <div class="more-insights">
                  <div class="row">
                    <div class="medium-12 columns">
                      <div class="text-center"><a id="more-insights-button" class="button dark-blue" data-tipo="<?php echo $tipo ?>" >CARREGAR MAIS <?php echo strtoupper($tipo . "S") ?>&nbsp;&nbsp;&nbsp;<i class="fa fa-plus" aria-hidden="true"></i></a></div>
                    </div>
                  </div>
                </div>
                <?php endif;
              endif;
              $insights_output .= ob_get_clean();
            endif;
      endwhile;
      wp_reset_postdata();
    else :
      $insights_output .= "Nenhum insight encontrado.";
    endif;

    return $insights_output;
  }
endif;
add_shortcode( 'insights_list', 'list_insights' );

function PaginationInsights(){
    echo do_shortcode('[insights_list paged="'.$_POST['paged'].'" tipo="'.$_POST['tipo'].'" tema="'.$_POST['tema'].'" area_aplicacao="'.$_POST['area_aplicacao'].'"]');
    die();
}
add_action( 'wp_ajax_nopriv_PaginationInsights', 'PaginationInsights' );
add_action( 'wp_ajax_PaginationInsights', 'PaginationInsights' );