<?php
/**
 * Shortcode for insights list on insights pages
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'search_insights' ) ) :
  function search_insights() {
    $search_output = NULL;
    $tema = $area_aplicacao = $area_industria = NULL;

    $tema           = $_GET['tema'];
    $area_aplicacao = $_GET['area_aplicacao'];
    $area_industria = $_GET['area_industria'];


    ob_start(); ?>
      <form>
        <div class="row">
          <div class="medium-3 columns">
            <?php
              $field_key = "field_591e13a54d4e6";
              $field = get_field_object($field_key);

              if( $field ) {
                  echo '<select name="tema">';
                  echo '<option value="0">' . $field['label'] . '</option>';
                  foreach( $field['choices'] as $k => $v ) {
                    $selected = ($k === $tema) ? "selected" : "";
                    echo '<option value="' . $k . '" ' . $selected . '>' . $v . '</option>';
                  }
                echo '</select>';
              }
            ?>
          </div>
          <div class="medium-3 columns">
            <?php
              $field_key = "field_591e14ca6cc18";
              $field = get_field_object($field_key);

              if( $field ) {
                echo '<select name="area_aplicacao">';
                  echo '<option value="0">' . $field['label'] . '</option>';
                  foreach( $field['choices'] as $k => $v ) {
                    $selected = ($k === $area_aplicacao) ? "selected" : "";
                    echo '<option value="' . $k . '" ' . $selected . '>' . $v . '</option>';
                  }
                echo '</select>';
              }
            ?>
          </div>
          <div class="medium-3 columns">
            <?php
              $field_key = "field_591e15a46313a";
              $field = get_field_object($field_key);

              if( $field ) {
                echo '<select name="area_industria">';
                  echo '<option value="0">' . $field['label'] . '</option>';
                  foreach( $field['choices'] as $k => $v ) {
                    $selected = ($k === $area_industria) ? "selected" : "";
                    echo '<option value="' . $k . '" ' . $selected . '>' . $v . '</option>';
                  }
                echo '</select>';
              }
            ?>
          </div>
          <div class="medium-3 columns">
            <button type="submit" class="expanded button">Procurar</button>
          </div>
        </div>
      </form>

    <?php
    $search_output .= ob_get_clean();
    return $search_output;
  }
endif;
add_shortcode( 'insights_search', 'search_insights' );