<?php
/**
 * The template for displaying the header of tour page
 */
?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
  <?php do_action( 'foundationpress_after_body' ); ?>

  <?php do_action( 'foundationpress_layout_start' ); ?>

  <header id="masthead" class="site-header tour">
    <div class="top-bar" style="width:100%">
      <div class="row">
        <div class="small-12 columns">
          <div class="top-bar-title text-center">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/identity/logo-lecom.png" srcset="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/identity/logo-lecom@2x.png 2x, <?php echo get_stylesheet_directory_uri(); ?>/assets/images/identity/logo-lecom@3x.png 3x" alt="Lecom"></a>
          </div>
        </div>
      </div>
    </div>
  </header>
  <?php do_action( 'foundationpress_after_header' );


