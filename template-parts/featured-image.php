<?php
  global $post;
  $postid = $post->ID;
  $page_slug = $post->post_name;
  $featured_image_id = NULL;
  $args = array(
    'post_type' => 'header_banners',
    'posts_per_page' => -1
  );
  $loop = new WP_Query( $args );

  while ( $loop->have_posts() ) : $loop->the_post();
    if (!in_array($postid, get_field('header_page'))) {
         continue;
    }

    if( have_rows('banners') ): ?>
      <header id="featured-hero" class="<? echo (count(get_field('banners')) > 1) ? "banner-slider " : ""; echo "page-" . $page_slug; ?>">
        <?php while ( have_rows('banners') ) : the_row();

        $featured_image_id = get_sub_field('banner_image');

        if ($featured_image_id) {
          $featured_small   = wp_get_attachment_image_src($featured_image_id, "featured-small" );
          $featured_medium  = wp_get_attachment_image_src($featured_image_id, "featured-medium" );
          $featured_large   = wp_get_attachment_image_src($featured_image_id, "featured-large" );
          $featured_xlarge  = wp_get_attachment_image_src($featured_image_id, "featured-xlarge" );

          $data_interchange = "data-interchange=\"[" . $featured_small[0] . ", small], [" . $featured_medium[0] . ", medium], [" . $featured_large[0] . ", large], [" . $featured_xlarge[0] .", xlarge]\"";
        }
        ?>
        <div class="banner-hero" <?php echo $data_interchange; ?>>
          <div class="banner-row">
            <div class="banner-col" data-magellan>
              <div class="banner-title"><?php the_sub_field('banner_title'); ?></div>
              <span class="banner-subtitle"><?php the_sub_field('banner_subtitle'); ?></span>
              <?php
                $video_author = get_sub_field('video_author');
                switch ($video_author) {
                    case "video": ?>
                        <a href="<?php echo get_sub_field('banner_video_link') ?>" class="button secondary">Veja o vídeo</a>
                    <?php break;
                    case "autor": ?>
                        <span class="banner-author"><?php the_sub_field('banner_author_name'); ?></span>
                        <span class="banner-author-office"><?php the_sub_field('banner_author_office'); ?></span>
                    <?php break;
                    case "link": ?>
                        <a href="<?php echo get_sub_field('banner_link'); ?>" class="button banner"><?php echo get_sub_field('banner_link_text'); ?> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/identity/round-chevron-down-white.png" alt="&#x2C5;"></a>
                    <?php break;
                    case "logo": ?>
                      <img class="banner-logo" src="<?php echo get_sub_field('banner_logo')["url"]; ?>" alt="<?php echo get_sub_field('banner_logo')["alt"]; ?>">
                    <?php break;
                    case "nenhum":
                    break;
                }
              ?>
              </div>
            </div>
          </div>
      <?php endwhile; ?>
      </header>
    <?php endif;
  endwhile;
wp_reset_postdata();
