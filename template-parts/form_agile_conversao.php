<?php
/**
 * Created by PhpStorm.
 * User: Fabio bueno
 * Date: 12/04/2017
 * Time: 09:59
 */
extract( $wp_query->query_vars );
?>

<?php if (isset($_POST['_agile_redirect_url'])): ?>
    <script>
        var urlRedirectForm = "<?=$_POST['_agile_redirect_url'];?>";
        location.replace(urlRedirectForm);
    </script>
<?php endif; ?>

<div class="reveal large" id="<?= $id_modal ?>" data-reveal>
    <a href="http://lecom.com.br/LecomModeler1.9.2-Install.zip" download  target="_blank"  id="downloadModeler" hidden></a>
    <div class="row">
        <div class="medium-10 large-8 small-centered columns">
            <?php if ($tags_agile == 'BPM Studio' || $tags_agile == 'Workspace'){?>
                <p class="highlight-text-agile">Preencha os seus dados corretamente.</p>
            <?php }else{ ?>
                <p class="highlight-text-agile">Preencha os seus dados corretamente.</p>
            <?php } ?>
        </div>
    </div>
    <div class="alert callout alert-preencha-campos" data-closable style="display: none;">
        <p style="color:#333;">Por favor preencha todos os campos obrigatorios</p>
        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <form class="form-view  agile-form-blck-transparent  theme5 form-padrao-agile form_<?=$id_modal?>" id="agile-form<?= ($dois_models == "true") ? '1':'' ?>"  action="" method="POST">
        <div style="display: none; height: 0px; width: 0px;">
            <input type="hidden" id="_agile_redirect_url" name="_agile_redirect_url" value="<?php echo $url_redirect;?>?lead=true&modeler=<?php echo $modeler; ?>">
            <input type="hidden" id="_agile_form_id_tags" name="tags" value='Lecom, <?php echo $tags_agile ?>'>
            <input type="hidden" value="<?=$dois_models?>">
            <input type="hidden" name="identificador_tracker_lahar" id="identificador_tracker_lahar" value="">
        </div>
        <div class="row">
            <div class="medium-6 columns">
                <div class="agile-group required-control">
                    <label class="agile-label" for="nome-1">Nome *</label>
                    <div class="agile-field-xlarge agile-field">
                        <input maxlength="250" id="nome-1" name="first_name" type="text" placeholder="" class="agile-height-default" required="">
                    </div>
                    <div class="agile-custom-clear"></div>
                </div>
            </div>
            <div class="medium-6 columns">
                <div class="agile-group required-control">
                    <label class="agile-label" for="Sobrenome">Sobrenome *</label>
                    <div class="agile-field-xlarge agile-field">
                        <input maxlength="250" id="Sobrenome" name="last_name" type="text" placeholder="" class="agile-height-default" required="">
                    </div>
                    <div class="agile-custom-clear"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="medium-6 columns">
                <div class="agile-group required-control">
                    <label class="agile-label" for="Empresa">Empresa *</label>
                    <div class="agile-field-xlarge agile-field">
                        <input maxlength="250" id="Empresa" name="company" type="text" placeholder="" class="agile-height-default" required="">
                    </div>
                    <div class="agile-custom-clear"></div>
                </div>
            </div>
            <div class="medium-6 columns">
                <div class="agile-group required-control">
                    <label class="agile-label" for="Telefone">Telefone *</label>
                    <div class="agile-field-xlarge agile-field">
                        <input maxlength="250" id="Telefone" name="phone" type="text" placeholder="(99)99999-9999" class="agile-height-default" required="">
                    </div>
                    <div class="agile-custom-clear"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="medium-6 columns">
                <div class="agile-group required-control">
                    <label class="agile-label" for="E-mail">E-mail *</label>
                    <div class="agile-field-xlarge agile-field">
                        <input maxlength="250" id="E-mail" name="email" type="email" placeholder="exemplo@exemplo.com.br" class="agile-height-default" required="">
                    </div>
                    <div class="agile-custom-clear"></div>
                </div>
            </div>
            <div class="medium-6 columns">
                <div class="agile-group">
                    <label class="agile-label" for="Pais">Pais</label>
                    <div class="agile-field-xlarge agile-field">
                        <select id="Pais" name="country" class="agile-height-default" required="">
                            <option value="">Selecione</option>
                            <option value="Afeganistão">Afeganistão</option>
                            <option value="África do Sul">África do Sul</option>
                            <option value="Albânia">Albânia</option>
                            <option value="Argélia">Argélia</option>
                            <option value="Alemanha">Alemanha</option>
                            <option value="Andorra">Andorra</option>
                            <option value="Angola">Angola</option>
                            <option value="Arábia Saudita">Arábia Saudita</option>
                            <option value="Argentina">Argentina</option>
                            <option value="Arménia">Arménia</option>
                            <option value="Antiga e Barbuda">Antiga e Barbuda</option>
                            <option value="Austrália">Austrália</option>
                            <option value="Áustria">Áustria</option>
                            <option value="Azerbaijão">Azerbaijão</option>
                            <option value="Baamas">Baamas</option>
                            <option value="Barém">Barém</option>
                            <option value="Bangladexe">Bangladexe</option>
                            <option value="Barbados">Barbados</option>
                            <option value="Bélgica">Bélgica</option>
                            <option value="Belize">Belize</option>
                            <option value="Benim">Benim</option>
                            <option value="Butão">Butão</option>
                            <option value="Bielorrússia">Bielorrússia</option>
                            <option value="Mianmar">Mianmar</option>
                            <option value="Bolívia">Bolívia</option>
                            <option value="Bósnia e Herzegovina">Bósnia e Herzegovina</option>
                            <option value="Botsuana">Botsuana</option>
                            <option value="Brazil">Brasil</option>
                            <option value="Brunei">Brunei</option>
                            <option value="Bulgária">Bulgária</option>
                            <option value="Burquina Faso">Burquina Faso</option>
                            <option value="Burúndi">Burúndi</option>
                            <option value="Camboja">Camboja</option>
                            <option value="Camarões">Camarões</option>
                            <option value="Canadá">Canadá</option>
                            <option value="Cabo Verde">Cabo Verde</option>
                            <option value="Chile">Chile</option>
                            <option value="China">China</option>
                            <option value="Chipre">Chipre</option>
                            <option value="Colômbia">Colômbia</option>
                            <option value="Comores">Comores</option>
                            <option value="Congo-Brazzaville">Congo-Brazzaville</option>
                            <option value="Coreia do Norte">Coreia do Norte</option>
                            <option value="Coreia do Sul">Coreia do Sul</option>
                            <option value="Costa Rica">Costa Rica</option>
                            <option value="Costa do Marfim">Costa do Marfim</option>
                            <option value="Croácia">Croácia</option>
                            <option value="Cuba">Cuba</option>
                            <option value="Dinamarca">Dinamarca</option>
                            <option value="Jibuti">Jibuti</option>
                            <option value="Dominica">Dominica</option>
                            <option value="Egito">Egito</option>
                            <option value="Emirados Árabes Unidos">Emirados Árabes Unidos</option>
                            <option value="Equador">Equador</option>
                            <option value="Eritreia">Eritreia</option>
                            <option value="Espanha">Espanha</option>
                            <option value="Estónia">Estónia</option>
                            <option value="Estados Unidos">Estados Unidos</option>
                            <option value="Etiópia">Etiópia</option>
                            <option value="Fiji">Fiji</option>
                            <option value="Finlândia">Finlândia</option>
                            <option value="França">França</option>
                            <option value="Gabão">Gabão</option>
                            <option value="Gâmbia">Gâmbia</option>
                            <option value="Geórgia">Geórgia</option>
                            <option value="Gana">Gana</option>
                            <option value="Grécia">Grécia</option>
                            <option value="Granada">Granada</option>
                            <option value="Guatemala">Guatemala</option>
                            <option value="Guiné">Guiné</option>
                            <option value="Guiné Equatorial">Guiné Equatorial</option>
                            <option value="Guiné-Bissau">Guiné-Bissau</option>
                            <option value="Guiana">Guiana</option>
                            <option value="Haiti">Haiti</option>
                            <option value="Honduras">Honduras</option>
                            <option value="Hungria">Hungria</option>
                            <option value="Maurícia">Maurícia</option>
                            <option value="Índia">Índia</option>
                            <option value="Indonésia">Indonésia</option>
                            <option value="Iraque">Iraque</option>
                            <option value="Irão">Irão</option>
                            <option value="Irlanda">Irlanda</option>
                            <option value="Islândia">Islândia</option>
                            <option value="Israel">Israel</option>
                            <option value="Itália">Itália</option>
                            <option value="Jamaica">Jamaica</option>
                            <option value="Japão">Japão</option>
                            <option value="Jordânia">Jordânia</option>
                            <option value="Cazaquistão">Cazaquistão</option>
                            <option value="Quénia">Quénia</option>
                            <option value="Quirguistão">Quirguistão</option>
                            <option value="Quiribáti">Quiribáti</option>
                            <option value="Cosovo">Cosovo</option>
                            <option value="Cuaite">Cuaite</option>
                            <option value="Laus">Laus</option>
                            <option value="Lesoto">Lesoto</option>
                            <option value="Letónia">Letónia</option>
                            <option value="Líbano">Líbano</option>
                            <option value="Libéria">Libéria</option>
                            <option value="Líbia">Líbia</option>
                            <option value="Listenstaine">Listenstaine</option>
                            <option value="Lituânia">Lituânia</option>
                            <option value="Luxemburgo">Luxemburgo</option>
                            <option value="Macedónia">Macedónia</option>
                            <option value="Madagáscar">Madagáscar</option>
                            <option value="Malásia">Malásia</option>
                            <option value="Maláui">Maláui</option>
                            <option value="Maldivas">Maldivas</option>
                            <option value="Mali">Mali</option>
                            <option value="Malta">Malta</option>
                            <option value="Marrocos">Marrocos</option>
                            <option value="Ilhas Marechal">Ilhas Marechal</option>
                            <option value="Mauritânia">Mauritânia</option>
                            <option value="México">México</option>
                            <option value="Micronésia">Micronésia</option>
                            <option value="Moldávia">Moldávia</option>
                            <option value="Mónaco">Mónaco</option>
                            <option value="Mongólia">Mongólia</option>
                            <option value="Montenegro">Montenegro</option>
                            <option value="Moçambique">Moçambique</option>
                            <option value="Namíbia">Namíbia</option>
                            <option value="Nauru">Nauru</option>
                            <option value="Nepal">Nepal</option>
                            <option value="Nicarágua">Nicarágua</option>
                            <option value="Níger">Níger</option>
                            <option value="Nigéria">Nigéria</option>
                            <option value="Noruega">Noruega</option>
                            <option value="Nova Zelândia">Nova Zelândia</option>
                            <option value="Omã">Omã</option>
                            <option value="Uganda">Uganda</option>
                            <option value="Usbequistão">Usbequistão</option>
                            <option value="Paquistão">Paquistão</option>
                            <option value="Palau">Palau</option>
                            <option value="Panamá">Panamá</option>
                            <option value="Papua Nova Guiné">Papua Nova Guiné</option>
                            <option value="Paraguai">Paraguai</option>
                            <option value="Países Baixos">Países Baixos</option>
                            <option value="Peru">Peru</option>
                            <option value="Filipinas">Filipinas</option>
                            <option value="Polónia">Polónia</option>
                            <option value="Portugal">Portugal</option>
                            <option value="Catar">Catar</option>
                            <option value="República Centro-Africana">República Centro-Africana</option>
                            <option value="República Democrática do Congo">República Democrática do Congo</option>
                            <option value="República Dominicana">República Dominicana</option>
                            <option value="República Checa">República Checa</option>
                            <option value="Roménia">Roménia</option>
                            <option value="Reino Unido">Reino Unido</option>
                            <option value="Rússia">Rússia</option>
                            <option value="Ruanda">Ruanda</option>
                            <option value="Santa Lúcia">Santa Lúcia</option>
                            <option value="São Cristóvão e Neves">São Cristóvão e Neves</option>
                            <option value="São Marinho">São Marinho</option>
                            <option value="São Vicente e Granadinas">São Vicente e Granadinas</option>
                            <option value="Salomão">Salomão</option>
                            <option value="Salvador">Salvador</option>
                            <option value="Samoa">Samoa</option>
                            <option value="São Tomé e Príncipe">São Tomé e Príncipe</option>
                            <option value="Senegal">Senegal</option>
                            <option value="Sérvia">Sérvia</option>
                            <option value="Seicheles">Seicheles</option>
                            <option value="Serra Leoa">Serra Leoa</option>
                            <option value="Singapura">Singapura</option>
                            <option value="Eslováquia">Eslováquia</option>
                            <option value="Eslovénia">Eslovénia</option>
                            <option value="Somália">Somália</option>
                            <option value="Sudão">Sudão</option>
                            <option value="Sudão do Sul">Sudão do Sul</option>
                            <option value="Sri Lanca">Sri Lanca</option>
                            <option value="Suécia">Suécia</option>
                            <option value="Suíça">Suíça</option>
                            <option value="Suriname">Suriname</option>
                            <option value="Suazilândia">Suazilândia</option>
                            <option value="Síria">Síria</option>
                            <option value="Tajiquistão">Tajiquistão</option>
                            <option value="Taiuã">Taiuã</option>
                            <option value="Tanzânia">Tanzânia</option>
                            <option value="Chade">Chade</option>
                            <option value="Tailândia">Tailândia</option>
                            <option value="Timor-Leste">Timor-Leste</option>
                            <option value="Togo">Togo</option>
                            <option value="Tonga">Tonga</option>
                            <option value="Trindade e Tobago">Trindade e Tobago</option>
                            <option value="Tunísia">Tunísia</option>
                            <option value="Turcomenistão">Turcomenistão</option>
                            <option value="Turquia">Turquia</option>
                            <option value="Tuvalu">Tuvalu</option>
                            <option value="Ucrânia">Ucrânia</option>
                            <option value="Uruguai">Uruguai</option>
                            <option value="Vanuatu">Vanuatu</option>
                            <option value="Vaticano">Vaticano</option>
                            <option value="Venezuela">Venezuela</option>
                            <option value="Vietname">Vietname</option>
                            <option value="Iémen">Iémen</option>
                            <option value="Zâmbia">Zâmbia</option>
                            <option value="Zimbábue">Zimbábue</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="medium-6 columns">
                <div class="agile-group estado-input">
                    <label class="agile-label" for="Estado">Estado *</label>
                    <div class="agile-field-xlarge agile-field">
                        <select id="Estado" name="state" class="agile-height-default" required="">
                            <option value="">Selecione</option>
                            <option value="Alagoas">Alagoas</option>
                            <option value="Amapá">Amapá</option>
                            <option value="Amazonas">Amazonas</option>
                            <option value="Bahia">Bahia</option>
                            <option value="Ceará">Ceará</option>
                            <option value="Distrito Federal">Distrito Federal</option>
                            <option value="Espírito Santo">Espírito Santo</option>
                            <option value="Maranhão">Maranhão</option>
                            <option value="Mato Grosso">Mato Grosso</option>
                            <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
                            <option value="Minas Gerais">Minas Gerais</option>
                            <option value="Pará">Pará</option>
                            <option value="Paraíba">Paraíba</option>
                            <option value="Paraná">Paraná</option>
                            <option value="Pernambuco">Pernambuco</option>
                            <option value="Piauí">Piauí</option>
                            <option value="Roraima">Roraima</option>
                            <option value="Rondônia">Rondônia</option>
                            <option value="Rio de Janeiro">Rio de Janeiro</option>
                            <option value="Rio Grande do Norte">Rio Grande do Norte</option>
                            <option value="Rio Grande do Sul">Rio Grande do Sul</option>
                            <option value="Santa Catarina">Santa Catarina</option>
                            <option value="São Paulo">São Paulo</option>
                            <option value="Sergipe">Sergipe</option>
                            <option value="Tocantins">Tocantins</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="medium-6 columns">
                <div class="agile-group cidade-input">
                    <label class="agile-label" for="Cidade">Cidade</label>
                    <div class="agile-field-xlarge agile-field">
                        <input maxlength="250" id="Cidade" name="city" type="text" placeholder="" class="agile-height-default">
                    </div>
                    <div class="agile-custom-clear"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="medium-6 columns">
                <div class="agile-group">
                    <label class="agile-label" for="area-atuacao">Área de atuação da empresa *</label>
                    <div class="agile-field-xlarge agile-field">
                        <select id="area-atuacao" name="tags" class="agile-height-default" required="">
                            <option value="">Selecione</option>
                            <option value="Saúde">Saúde</option>
                            <option value="Governo">Governo</option>
                            <option value="Financeiro">Financeiro</option>
                            <option value="Manufatura">Manufatura</option>
                            <option value="Varejo">Varejo</option>
                            <option value="Montadoras">Montadoras</option>
                            <option value="Entidades e Ongs">Entidades e Ongs</option>
                            <option value="Outros">Outros</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="medium-6 columns">
                <div class="agile-group">
                    <label class="agile-label" for="Setor de atuação">Setor de atuação *</label>
                    <div class="agile-field-xlarge agile-field">
                        <select id="setor-de-atuacao" name="tags" class="agile-height-default" required="">
                            <option value="">Selecione</option>
                            <option value="Mkt">Mkt</option>
                            <option value="Vendas e Negócios">Vendas e Negócios</option>
                            <option value="RH">RH</option>
                            <option value="Supply Chain">Supply Chain</option>
                            <option value="TI">TI</option>
                            <option value="Operações">Operações</option>
                            <option value="Financeiro">Financeiro</option>
                            <option value="Estudante">Estudante</option>
                            <option value="Outros">Outros</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="medium-12 columns">
                <? if ($dois_models == "false"){ ?>
                <!--recaptcha aglignment--><div class="agile-group"><label class="agile-label"></label><div class="agile-field-xlarge agile-field"><div class="g-recaptcha" style="transform:scale(0.91);-webkit-transform:scale(0.91);transform-origin:0 0;-webkit-transform-origin:0 0; width="304px";" data-sitekey="6Lfe9BIUAAAAABQUMXGYyL_TbiaC8jUkvLXGvf2c" data-callback="agileGCaptchaOnSuccess"></div></div></div>
                <? }else{ ?>
                    <div class="g-recaptcha" data-sitekey="6Lfe9BIUAAAAABQUMXGYyL_TbiaC8jUkvLXGvf2c"></div>
                <? } ?>
            <!-- Button -->
            <div class="agile-group float-right">
                <label class="agile-label">&nbsp;</label>
                <div class="agile-field agile-button-field">
                    <button type="submit" class="agile-button button">Enviar</button>
                    <br><span id="agile-error-msg"></span>
                </div>
            </div>
        </div>
    </form>
</div>

<?php if ($dois_models == 'false'): ?>
<!--    <script src="https://www.google.com/recaptcha/api.js"></script>-->
    <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
<?php endif; ?>

<?php if ($dois_models == 'false'): ?>
    <script>
        var idForm = '#<?= $id_modal ?>';
        var formAgile = '<?= $id_modal ?>';
        var urlRedirect = '<?= $url_redirect?>';
    </script>

    <script>
        $( document ).ready(function() {

            $('.form_'+formAgile + " #Pais").change(function () {
                if ($(this).val() != "Brazil") {
                    $(".estado-input").hide();
                    $("#Estado").removeAttr("required");

                    $(".cidade-input").hide();
                    $("#Cidade").removeAttr("required");
                } else {
                    $(".estado-input").show();
                    $("#Estado").attr("required", "required");

                    $(".cidade-input").show();
                    $("#Cidade").attr("required", "required");
                }
            });

            $('.form_'+formAgile + " #Telefone").mask(SPMaskBehavior, spOptions);

            $('.form_'+formAgile + " #Pais").val("Brazil");
            function getUrlVars() {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf("?") + 1).split("&");
                for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split("=");
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                return vars;
            }

            var lead = getUrlVars()["lead"];
            var modeler = getUrlVars()["modeler"];
            if (lead == "true") {

                if(urlRedirect == 'http://lecom2017.localhost/entrega-agil-de-processos-digitais' || urlRedirect == 'http://lecom2017aceite.lecom.com.br/entrega-agil-de-processos-digitais' || urlRedirect == 'http://www.lecom.com.br/entrega-agil-de-processos-digitais' || urlRedirect == 'http://lecom.com.br/entrega-agil-de-processos-digitais'){
                    alert("Obrigado! Em breve você receberá no seu email o link para o webinar");
                }else{
                    alert("Obrigado, em breve entraremos em contato!");
                }


                if(modeler == "true"){
                    document.getElementById('downloadModeler').click();
                }

            }

            /*Formata os campos do formulario para adaptar para a Lahar*/
            function formatarLahar(form){
                var valores = {};
                valores['url_origem'] = window.location.href;
                valores['token_api_lahar'] = 'lecomOe5dsMSbyoIgGBfv5Wc4e4a6j6IbQg6DkwFHysEBqf6BsaV2KiSiajic';
                valores['nome_formulario'] = document.title;
                valores['identificador_tracker_lahar'] = $('#identificador_tracker_lahar').val();
                valores['email_contato'] = form['email'];
                valores['nome_contato'] = form['first_name'] + ' ' + form['last_name'];
                valores['tel_celular'] = form['phone'];
                valores['nome_empresa'] = form['company'];
                valores['cidade'] = form['city'];
                valores['estado'] = form['state'];
                valores['tags'] = form['tags'];

                return valores;
            }

            /*Ajax Lahar*/
            $(idForm +' button.agile-button').click(function(e){
                $('.form_'+formAgile +' .agile-button-field').addClass('loading_contact');
                e.preventDefault(e); /*Lahar          Salva os dados do input em 'valores' e envia para formatar o conteudo dos dados          */
                var inputs = $('.form_'+formAgile).find(':input');
                var valores = {};
                inputs.each(function() {
                    valores[this.name] = $(this).val();
                });
                valores['tags'] = $('.form_'+formAgile +'  #_agile_form_id_tags').val()+', '+$('.form_'+formAgile +'  #area-atuacao').val()+', '+$('.form_'+formAgile +'  #setor-de-atuacao').val();
                valores = formatarLahar(valores);

                if(valores.email_contato == '' || valores.nome_empresa == '' || valores.tel_celular == '' || valores.nome_contato == '' || valores.estado == '' || $('.form_'+formAgile + '  #area-atuacao').val() == '' || $('.form_'+formAgile + '  #setor-de-atuacao').val() == ''){
                    $('.alert-preencha-campos').show();
                    $('.form_'+formAgile +' .agile-button-field').removeClass('loading_contact');
                }else{
                    /*Envia ajax*/
                    $.ajax({
                        url: "https://app.lahar.com.br/api/conversions",
                        type: "POST",
                        async: true,
                        data: jQuery.param(valores),
                        complete: function(data){
                            $('#agile-form').submit();
                        }
                    });
                }
            });
        });
    </script>
    <script>
        var CaptchaCallback = function() {
            $('.g-recaptcha').each(function(index, el) {
                grecaptcha.render(el, {'sitekey' : '6Lfe9BIUAAAAABQUMXGYyL_TbiaC8jUkvLXGvf2c'});
            });
        };
    </script>


<?php endif; ?>

<?php if ($dois_models == 'true'): ?>
    <script>
        console.log('teste');
        var idForm2 = '#<?= $id_modal ?>';
        var formAgile2 = '<?= $id_modal ?>';
        var urlRedirect2 = '<?= $url_redirect?>';
    </script>

    <script>
        $( document ).ready(function() {
            $('.form_'+formAgile2 + " #Pais").change(function () {
                if ($(this).val() != "Brazil") {
                    $(".estado-input").hide();
                    $("#Estado").removeAttr("required");

                    $(".cidade-input").hide();
                    $("#Cidade").removeAttr("required");
                } else {
                    $(".estado-input").show();
                    $("#Estado").attr("required", "required");

                    $(".cidade-input").show();
                    $("#Cidade").attr("required", "required");
                }
            });

            $('.form_'+formAgile2 + " #Telefone").mask(SPMaskBehavior, spOptions);

            $('.form_'+formAgile2 + " #Pais").val("Brazil");
            function getUrlVars() {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf("?") + 1).split("&");
                for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split("=");
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                return vars;
            }

            var lead = getUrlVars()["lead"];
            var modeler = getUrlVars()["modeler"];
            if (lead == "true") {
                if(modeler == "true"){
                }

            }

            /*Formata os campos do formulario para adaptar para a Lahar*/
            function formatarLahar(form){
                var valores = {};
                valores['url_origem'] = window.location.href;
                valores['token_api_lahar'] = 'lecomOe5dsMSbyoIgGBfv5Wc4e4a6j6IbQg6DkwFHysEBqf6BsaV2KiSiajic';
                valores['identificador_tracker_lahar'] = $('#identificador_tracker_lahar').val();
                valores['nome_formulario'] = document.title;
                valores['email_contato'] = form['email'];
                valores['nome_contato'] = form['first_name'] + ' ' + form['last_name'];
                valores['tel_celular'] = form['phone'];
                valores['nome_empresa'] = form['company'];
                valores['cidade'] = form['city'];
                valores['estado'] = form['state'];
                valores['tags'] = form['tags'];

                return valores;
            }

            /*Ajax Lahar*/
            $(idForm2 +' button.agile-button').click(function(e){
                $('.form_'+formAgile2 +' .agile-button-field').addClass('loading_contact');
                e.preventDefault(e); /*Lahar          Salva os dados do input em 'valores' e envia para formatar o conteudo dos dados          */
                var inputs = $('.form_'+formAgile2).find(':input');
                var valores = {};
                inputs.each(function() {
                    valores[this.name] = $(this).val();
                });
                valores['tags'] = $('.form_'+formAgile2 +'  #_agile_form_id_tags').val()+', '+$('.form_'+formAgile2 +'  #area-atuacao').val()+', '+$('.form_'+formAgile2 +'  #setor-de-atuacao').val();
                valores = formatarLahar(valores);

                if(valores.email_contato == '' || valores.nome_empresa == '' || valores.tel_celular == '' || valores.nome_contato == '' || valores.estado == '' || $('.form_'+formAgile2 + '  #area-atuacao').val() == '' || $('.form_'+formAgile2 + '  #setor-de-atuacao').val() == ''){
                    $('.alert-preencha-campos').show();
                    $('.form_'+formAgile2 +' .agile-button-field').removeClass('loading_contact');
                }else{
                    /*Envia ajax*/
                    $.ajax({
                        url: "https://app.lahar.com.br/api/conversions",
                        type: "POST",
                        async: true,
                        data: jQuery.param(valores),
                        complete: function(data){
                            $('#agile-form1').submit();
                        }
                    });
                }
            });
        });
    </script>
    <script>
        var CaptchaCallback = function() {
            $('.g-recaptcha').each(function(index, el) {
                grecaptcha.render(el, {'sitekey' : '6Lfe9BIUAAAAABQUMXGYyL_TbiaC8jUkvLXGvf2c'});
            });
        };
    </script>


<?php endif; ?>




