<?php
/**
 * Template part for right menu bar
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div id="canais-de-comunicacao" class="canais">
  <ul>
    <li><a href="/contato" title="Contato"><img src="<?php echo get_template_directory_uri() . "/assets/images/pages/all/ic-contato.png"; ?>" alt="">CONTATO</a></li>
    <li><a class="link-chat" href="#chat" title="Chat"><img src="<?php echo get_template_directory_uri() . "/assets/images/pages/all/ic-chat.png"; ?>" alt="">CHAT</a></li>
    <li><a class="link-social" href="#social" title="Social"><img src="<?php echo get_template_directory_uri() . "/assets/images/pages/all/ic-social.png"; ?>" alt="">SOCIAL</a></li>
    <li><a href="/programa-de-parceiros/" title="Parceiros"><img src="<?php echo get_template_directory_uri() . "/assets/images/pages/all/ic-parceiros.png"; ?>" alt="">PARCEIROS</a></li>
  </ul>
  <div id="social" class="social">
    <span class="social-text">INSCREVA-SE NOS CANAIS SOCIAIS DA LECOM</span>
    <div class="redes-sociais">
      <a href="https://www.youtube.com/user/lecomsa" title="Canal no YouTube da Lecom" target="_blank">
        <span class="fa-stack fa-lg">
          <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
          <i class="fa fa-youtube-play fa-stack-1x"></i>
        </span>
      </a>

      <a href="https://www.facebook.com/lecomtecnologia" title="Facebook da Lecom" target="_blank">
        <span class="fa-stack fa-lg">
          <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
          <i class="fa fa-facebook fa-stack-1x"></i>
        </span>
      </a>

      <a href="https://www.linkedin.com/company/lecom" title="LinkedIn da Lecom" target="_blank">
        <span class="fa-stack fa-lg">
          <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
          <i class="fa fa-linkedin fa-stack-1x"></i>
        </span>
      </a>

      <a href="http://www.bloglecom.com.br/" title="Blog da Lecom" target="_blank">
        <span class="fa-stack fa-lg">
          <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
          <i class="fa fa-wordpress fa-stack-1x"></i>
        </span>
      </a>

      <a href="https://twitter.com/LecomTecnologia" title="Twitter da Lecom" target="_blank">
        <span class="fa-stack fa-lg">
          <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
          <i class="fa fa-twitter fa-stack-1x"></i>
        </span>
      </a>

      <a href="http://issuu.com/lecomsa" title="Issu da Lecom" target="_blank">
        <span class="fa-stack fa-lg">
          <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
          <i class="ic-l ic-issu fa-stack-1x"></i>
        </span>
      </a>
    </div>
  </div>
</div>
