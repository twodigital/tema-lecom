$(function() {
  if ($(".cases-slider").length) {
    $('.cases-slider').removeClass("hide").bxSlider({ controls: false, adaptiveHeight: true, auto: true, mode: 'fade', speed: 1000, pause: 10000, preloadImages: 'all', autoHover: true });
  }
});