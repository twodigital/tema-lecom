
var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };

function efetua_integracao_chat(nome, email, telefone) {
    var campos = { // Colocar aqui campos fixos ou enviar estas infos como hidden no formulário
        token_api_lahar: 'lecomOe5dsMSbyoIgGBfv5Wc4e4a6j6IbQg6DkwFHysEBqf6BsaV2KiSiajic',
        nome_formulario: 'chat-site', // Para integração 'conversions'
        url_origem: $(location).attr('href'), // Alterar apenas se necessário
        email_contato: email,
        nome_contato: nome,
        tel_fixo: telefone,
        tags: 'chat'
    };

    $.ajax({
        url: "https://app.lahar.com.br/api/conversions",
        type: "POST",
        async: true,
        crossDomain: true,
        data: jQuery.param(campos),
        complete: function (data) {
            console.log('funcionou');
        }
    });
}


$( document ).ready(function() {

    if ($("#contato").length) {
        $('.tel>input.wpcf7-tel').mask(SPMaskBehavior, spOptions);

        $(".wpcf7").on('wpcf7:mailsent', function(event){
            $('.input-contact').removeClass('loading_contact');
        });

        $(".wpcf7").on('wpcf7:mailfailed', function(event){
            $('.input-contact').removeClass('loading_contact');
        });
    }

    $zopim(function () {
        $zopim.livechat.setOnChatStart(function () {
            var name;
            var email;
            var phone;

            name = $zopim.livechat.getName();
            email = $zopim.livechat.getEmail();
            phone = $zopim.livechat.getPhone();

            if (email) {
                efetua_integracao_chat(name, email, phone);
            }
        });
    });

});