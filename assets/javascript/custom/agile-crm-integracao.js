/**
 * Created by Lecom on 09/03/2017.
 */
// <script id="_agile_min_js" async type="text/javascript" src="https://d1gwclp1pmzk26.cloudfront.net/agile/agile-cloud.js"></script>
/*jshint sub:true*/
var Agile_API = Agile_API || {}; Agile_API.on_after_load = function(){
_agile.set_account('togg0peddmkvorirres7u96s13', 'two-digital');
_agile_set_whitelist('dHdvLWRpZ2l0YWw=');
_agile.track_page_view();
_agile_execute_web_rules();};

function createLeadAgile($email,$nome,$sobrenome,$empresa,$telefone,$cidade,$estado,$tags,cb) {
    var email_received = $email;
    var contact = {};
    contact.email = email_received;
    contact.first_name = $nome;
    contact.last_name = $sobrenome;
    contact.company = $empresa;
    contact.phone = $telefone;
    var address = {
        "city":$cidade,
        "state": $estado,
        "country": "Brazil"
    };
    contact.tags = $tags;
    contact.type = 'work';

    _agile.create_contact(contact, {
        success:cb,
        error: cb
    });
}


function formatarLahar2(form){
    var valores = {};
    valores['url_origem'] = window.location.href;
    valores['token_api_lahar'] = 'lecomOe5dsMSbyoIgGBfv5Wc4e4a6j6IbQg6DkwFHysEBqf6BsaV2KiSiajic';
    valores['nome_formulario'] = document.title;
    valores['email_contato'] = form['email'];
    valores['nome_contato'] = form['first_name'] + ' ' + form['last_name'];
    valores['tel_celular'] = form['tel'];
    valores['nome_empresa'] = form['company'];
    valores['cidade'] = form['city'];
    valores['estado'] = form['state'];
    valores['tags'] = form['tags'];

    return valores;
}

/*Ajax Lahar*/
$('.wpcf7-submit').click(function(e){
    e.preventDefault(e); /*Lahar          Salva os dados do input em 'valores' e envia para formatar o conteudo dos dados          */
    var inputs = $('.wpcf7-form').find(':input');
    var valores = {};
    inputs.each(function() {
        valores[this.name] = $(this).val();
    });
    valores['tags'] = 'Lecom, Contato';

    if (!$('input[name="receber_news[]"]').is(":checked")){
        valores['tags'] += ", Lecom Descadastrado";
    }

    valores = formatarLahar2(valores);

    if(valores.email_contato === '' || valores.nome_empresa === '' || valores.tel_celular === '' || valores.nome_contato === '' || valores.estado === '' || $('#area-atuacao').val() === '' || $('#setor-de-atuacao').val() === ''){
        $('.alert-preencha-campos').show();
    }else{
        /*Envia ajax*/
        $.ajax({
            url: "https://app.lahar.com.br/api/conversions",
            type: "POST",
            async: true,
            crossDomain: true,
            data: jQuery.param(valores),
            beforeSend: function(){
                $('.input-contact').addClass('loading_contact');
            },
            complete: function(data){
                $(".wpcf7-form").submit();
                // $('.input-contact').removeClass('loading_contact');
            }
        });
    }
});

$('input[name="receber_news[]"]').attr('checked','checked');

//Agile
// $(".wpcf7-submit").click(function (e) {
//     e.preventDefault();
//     var nome = $('input[name="first_name"]').val();
//     var sobrenome = $('input[name="last_name"]').val();
//     var empresa = $('input[name="company"]').val();
//     var telefone = $('input[name="tel"]').val();
//     var email = $('input[name="email"]').val();
//     var cidade = $('input[name="city"]').val();
//     var estado = $('select[name="state"]').val();
//     var tags = "";
//     tags += $('select[name="company_field_of_action"]').val() + ', ';
//     tags += $('select[name="sector_of_action"]').val() + ', ';
//     tags += "Form Contato, Lecom, Lecom 2017";
//
//     if (!$('input[name="receber_news[]"]').is(":checked")){
//         tags += ", Lecom Descadastrado";
//     }
//
//     function cb(data) {
//
//         var contact = {};
//         contact.email = email;
//         contact.first_name = nome;
//         contact.last_name = sobrenome;
//         contact.company = empresa;
//         contact.phone = telefone;
//         var address = {
//             "city":cidade,
//             "state": estado,
//             "country": "Brazil"
//         };
//         contact.address = JSON.stringify(address);
//         contact.tags = tags;
//         contact.type = 'work';
//
//         _agile.set_email(email);
//
//         _agile.update_contact(contact, {
//             success: function (data) {
//                 // $(".wpcf7-form").submit();
//             },
//             error: function (data) {
//                 // $(".wpcf7-form").submit();
//             }
//         });
//     }
//
//     if(email !== null && nome !== null && sobrenome !== null){
//         var retorno = createLeadAgile(email, nome, sobrenome, empresa, telefone, cidade, estado, tags, cb);
//     }
//
// });