$(function() {
  if ($(".depoimentos-slider").length) {
    $('.depoimentos-slider').removeClass("hide").bxSlider({ controls: false, adaptiveHeight: true, auto: true, mode: 'fade', speed: 1000, pause: 10000, preloadImages: 'all', autoHover: true });
  }

  if ($(".depoimentos-parceiros-slider").length) {
    $('.depoimentos-parceiros-slider').removeClass("hide").bxSlider({ controls: false, adaptiveHeight: true, auto: true, mode: 'horizontal', speed: 1000, pause: 10000, preloadImages: 'all', autoHover: true, slideMargin: 200 });
  }
});