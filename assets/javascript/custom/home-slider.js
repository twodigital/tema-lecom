$(function() {
    if ($(".banner-slider").length) {
        $('.banner-slider').removeClass("hide").bxSlider({ controls: false, adaptiveHeight: true, auto: true, mode: 'fade', speed: 1000, pause: 5000, preloadImages: 'all', autoHover: true });
    }
    if ($(".solucoes-slider").length) {
        var solucoes_slider = $('.solucoes-slider').removeClass("hide").bxSlider({ pager: false, controls: false, adaptiveHeight: true, auto: false, mode: 'horizontal', speed: 1000, preloadImages: 'all', autoHover: true });
        $('.link-industrias').on("click", function(e) { e.preventDefault(); solucoes_slider.goToNextSlide(); });
        $('.link-aplicacoes').on("click", function(e) { e.preventDefault(); solucoes_slider.goToPrevSlide(); });
    }
});