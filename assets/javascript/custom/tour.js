$(function() {
    if ($(".acompanhe-slider").length) {
        $('.acompanhe-slider').removeClass("hide").bxSlider({ controls: false, adaptiveHeight: false, auto: false, mode: 'horizontal', speed: 1000, preloadImages: 'all', autoHover: true, pagerCustom: '#acompanhe-slider-controls' });
    }
    if ($(".aprove-slider").length) {
        $('.aprove-slider').removeClass("hide").bxSlider({ controls: false, adaptiveHeight: false, auto: false, mode: 'horizontal', speed: 1000, preloadImages: 'all', autoHover: true, pagerCustom: '#aprove-slider-controls' });
    }
    if ($(".consulte-slider").length) {
        $('.consulte-slider').removeClass("hide").bxSlider({ controls: false, adaptiveHeight: false, auto: false, mode: 'horizontal', speed: 1000, preloadImages: 'all', autoHover: true, pagerCustom: '#consulte-slider-controls' });
    }
    if ($("#tour").length) {
     // introJs().setOption("hintButtonLabel", "Fechar").addHints();
    }
});