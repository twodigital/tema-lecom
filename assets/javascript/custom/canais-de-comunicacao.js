$(function() {
  if ($("#canais-de-comunicacao").length) {
    $(".link-chat").on("click", function (e) {
      e.preventDefault();
      $(this).toggleClass("is-active").blur();
      $zopim(function() {
          $zopim.livechat.window.toggle();
        });
    });

    $(".link-social").on("click", function (e) {
      e.preventDefault();
      $(this).toggleClass("is-active").blur();
      $("#social").toggleClass("is-open");
    });
  }
});