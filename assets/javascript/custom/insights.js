/*jshint sub:true*/
var pageCalc = 2;

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

jQuery("#more-insights-button").click(function(){
    var tipo = $('#more-insights-button').data('tipo');
    jQuery.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: {
            action: 'PaginationInsights',
            paged: pageCalc,
            tema: getUrlVars()['tema'],
            area_aplicacao: getUrlVars()['area_aplicacao'],
            area_industria: getUrlVars()['area_industria'],
            tipo: tipo
        },
        beforeSend:function(){
            $('#more-insights-button').hide().after('<a style="background-color:transparent" class="button loading_contact"></a>');
        },
        success: function(data, textStatus, XMLHttpRequest){
            if (data != 'Nenhum insight encontrado.'){
                $(".insight-search-results .insight:last").after(data);
                $('#more-insights-button').show();
                $('.loading_contact').hide();
            }else{
                $('#more-insights-button').hide().after('<a class="button dark-blue disabled">'+data+'</a>');
                $('.loading_contact').hide();
            }

            pageCalc++;

        },
        error: function(MLHttpRequest, textStatus, errorThrown){
            alert(errorThrown);
        }
    });
});


