<?php
/*
Template Name: Full Width
*/
if ( is_page('tour')) :
  get_header('tour');
else :
  get_header();
endif; ?>

<div id="page-full-width" role="main">
  <?php do_action( 'foundationpress_before_content' ); ?>
  <?php while ( have_posts() ) : the_post(); ?>
    <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
      <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
      <?php get_template_part( 'template-parts/featured-image' ); ?>
      <?php get_template_part( 'template-parts/canais-de-comunicacao' ); ?>
      <?php the_content(); ?>
    </article>
  <?php endwhile;?>
  <?php do_action( 'foundationpress_after_content' ); ?>
</div>

<?php get_footer();
